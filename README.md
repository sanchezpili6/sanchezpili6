### Hi there 👋

- 🔭 I’m currently working on [Urbvan]
- 🌱 I’m currently learning React
- 😄 Pronouns: she/her
- ⚡ Fun fact: I want to be both an SE and a Mycologyst 

### My github stats:  

[![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=sanchezpili6&count_private=true&show_icons=true&theme=radical
)](https://github.com/sanchezpili6/github-readme-stats)

[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com?user=sanchezpili6&theme=radical&date_format=M%20j%5B%2C%20Y%5D)](https://git.io/streak-stats)

### Connect with me:

[<img align="left" alt="codeSTACKr | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
[<img align="left" alt="codeSTACKr | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/gmail.svg" />][mail]


[Urbvan]: https://urbvan.com/
[linkedin]: https://www.linkedin.com/in/sanchezpili6/
[mail]: mailto:sanchezpili6@gmail.com

